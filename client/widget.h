/**************************************************************************
**  File: widget.h
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 21.08.2019
**************************************************************************/

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void setPort(int port);

private:
    Ui::Widget *ui;
    QTcpSocket* m_pTcpSocket;
    quint16     m_nNextBlockSize;

    QString m_str;
    int m_action = 0;
    int m_port = 0;

    void initSocket();

private slots:
    void slotReadyRead();
    void slotError(const QAbstractSocket::SocketError &err);
    void slotSendToServer();
    void slotConnected();
};

#endif // WIDGET_H
