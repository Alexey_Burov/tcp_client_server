/**************************************************************************
**  File: widget.cpp
**  Enterprise: FR&PC JSC "R&PA "Mars" "
**  Author: Burov A.A.
**  Created: 21.08.2019
**************************************************************************/

#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QTime>

namespace {
static QString c_hostNmae = "127.0.0.1";
}

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    m_pTcpSocket = new QTcpSocket(this);
    m_pTcpSocket->connectToHost(c_hostNmae,m_port);
    connect(m_pTcpSocket, SIGNAL(connected()), SLOT(slotConnected()));
    connect(m_pTcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(slotError(QAbstractSocket::SocketError)));
    connect(ui->toolButton, SIGNAL(clicked()), SLOT(slotSendToServer()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::initSocket()
{

}

void Widget::setPort(int port)
{
    m_port = port;
}

void Widget::slotReadyRead()
{
    ui->label->setText(m_pTcpSocket->readAll());

    ui->comboBox->setEnabled(false);
    ui->spinBox->setEnabled(false);
    ui->toolButton->setEnabled(false);
}

void Widget::slotError(const QAbstractSocket::SocketError &err)
{
    QString strError =
            "Error: " + (err == QAbstractSocket::HostNotFoundError ?
                             "The host was not found." :
                             err == QAbstractSocket::RemoteHostClosedError ?
                                 "The remote host is closed." :
                                 err == QAbstractSocket::ConnectionRefusedError ?
                                     "The connection was refused." :
                                     QString(m_pTcpSocket->errorString())
                                     );
    qCritical() << strError;
}

void Widget::slotSendToServer()
{
    if(ui->spinBox->isEnabled())
        m_pTcpSocket->write(QByteArray::number(ui->spinBox->value()));
    else
       m_pTcpSocket->write(ui->comboBox->currentText().toLatin1());
    m_action = !m_action;
    ui->comboBox->setEnabled(m_action);
    ui->spinBox->setEnabled(!m_action);
}

void Widget::slotConnected()
{
    qDebug() << "connect";
}
