CONFIG += c++11

QT       += core network

TARGET = server

HEADERS += \
    worker.h \
    server.h \
    client.h

SOURCES += \
    main.cpp \
    server.cpp \
    worker.cpp \
    client.cpp
