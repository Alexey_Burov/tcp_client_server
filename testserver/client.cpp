#include <cstdint>

#include <QtCore/QDebug>
#include <QtCore/QtEndian>
#include <QtNetwork/QHostAddress>

#include "client.h"

namespace
{
#pragma pack(push, 1)
struct socks4request
{
    uint8_t version;
    uint8_t command;
    uint16_t port;
    uint32_t address;
    uint8_t end;
};

struct socks4ansver
{
    uint8_t empty = 0;
    uint8_t status;
    uint16_t field1 = 0;
    uint32_t field2 = 0;
};
#pragma pack(pop)

enum SocksStatus
{
    Granted = 0x5a,
    Failed = 0x5b,
    Failed_no_identd = 0x5c,
    Failed_bad_user_id = 0x5d
};
}
Client::Client(qintptr socketDescriptor, QObject* parent) :
    QObject(parent)
{
    m_client.setSocketDescriptor(socketDescriptor);

    connect(&m_client, &QTcpSocket::readyRead,
            this, &Client::onRequest);

    connect(&m_client,&QTcpSocket::disconnected,
            this, &Client::onClientDisconnected);

    connect(&m_world, &QTcpSocket::connected,
            this, &Client::sendSocksAnsver);

    connect(&m_world, &QTcpSocket::readyRead,
            this, &Client::world2client);

    connect(&m_world,&QTcpSocket::disconnected,
            this, &Client::onWorldDisconnected);
}

void Client::onRequest()
{
    QByteArray request = m_client.readAll();


    if(first == 0){
        first = request.toInt();
        qDebug() << "first" << m_client.socketDescriptor() << first;
        return;
    }

    if(second.isEmpty()){
        second = request;
        qDebug() << "second" << m_client.socketDescriptor() << second;
        return;
    }

    if(third == 0){
        third = request.toInt();
        qDebug() << "third" << m_client.socketDescriptor() << first;
    }

    QString str;
    if(second == "+")
        str = QString("%1 %2 %3 = %4").arg(first).arg(QString(second)).arg(third).arg(first + third);
    if(second == "-")
        str = QString("%1 %2 %3 = %4").arg(first).arg(QString(second)).arg(third).arg(first - third);
    if(second == "*")
        str = QString("%1 %2 %3 = %4").arg(first).arg(QString(second)).arg(third).arg(first * third);
    if(second == "/")
        str = QString("%1 %2 %3 = %4").arg(first).arg(QString(second)).arg(third).arg(first / third);

    qDebug() << str;
    m_client.write(str.toLatin1());

    disconnect(&m_client, &QTcpSocket::readyRead, this,
               &Client::onRequest);

    connect(&m_client, &QTcpSocket::readyRead, this,
            &Client::client2world);
}

void Client::sendSocksAnsver()
{
    socks4ansver ans;
    ans.status = Granted;
    m_client.write(reinterpret_cast<char*>(&ans), sizeof(ans));
    m_client.flush();
}

void Client::client2world()
{
    m_world.write(m_client.readAll());
}

void Client::world2client()
{
    m_client.write(m_world.readAll());
}

void Client::onClientDisconnected()
{
    m_world.flush();

    done();
}

void Client::onWorldDisconnected()
{
    m_client.flush();

    done();
}

void Client::done()
{
    m_client.close();
    m_world.close();

    deleteLater();
}
