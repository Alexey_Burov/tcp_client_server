#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>
#include <QtCore/QCommandLineOption>

#include "server.h"

namespace {
static const int c_port = 7654;
static const int c_threads = 4;
}

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("testserver");
    QCoreApplication::setApplicationVersion("0.1");
    qRegisterMetaType<qintptr>("qintptr");
    QCommandLineParser parser;
    parser.setApplicationDescription("Multi-threaded SOCKS4 server on Qt");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption portOption(QStringList() << "p" << "port", "port","port");
    parser.addOption(portOption);

    QCommandLineOption threadOption(QStringList() << "t" << "threads",
            "The number of worker threads", "count");
    parser.addOption(threadOption);

    parser.process(app);

    size_t port = parser.value(portOption).toUInt();
    if (port == 0)
    {
        port = c_port;
    }

    size_t threads = parser.value(threadOption).toUInt();
    if (threads == 0)
    {
        threads = c_threads;
    }

    Server server(threads);

    if (!server.listen(QHostAddress::LocalHost, port))
    {
        qCritical() << server.errorString();
        exit(1);
    }

    qDebug() << "Run server on" << port << "threads" << threads;
    return app.exec();
}
